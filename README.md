
# I3 wm tutorial
https://discovery.endeavouros.com/installation/i3-wm/2021/03/

# Default browser setting

https://forum.endeavouros.com/t/i3-default-browser-change/14381/13

- /etc/environment
```
BROWSER=google-chrome-stable
EDITOR=nvim
```


# Network Setup
- 
```
$ nmcli device wifi list
$ nmcli device wifi connect SSID_or_BSSID password 'password'
```


# bluetooth
- https://wiki.archlinux.org/title/Bluetooth
```
$ bluetoothctl

[bluetooth]# agent KeyboardOnly
[bluetooth]# default-agent
[bluetooth]# power on
[bluetooth]# scan on
[bluetooth]# pair 00:12:34:56:78:90
[bluetooth]# connect 00:12:34:56:78:90

```

# Sound

- http://dsparch.sciomagelab.com/2019/02/26/아치리눅스-세팅기-6-sound-설정/
- http://ders45.blogspot.com/2015/12/blog-post_61.html

```
$ pacman -S alsa alsa-utils


# 사운드 카드 장치 확인
cat /proc/asound/cards

# 사운드 카드 드라이버 초기화 및 백그라운드로 데몬 시작
$ alsactl init <사운드 카드 >
$ alsactl -b daemon <사운드 카드>

$ pactl set-sink-volume @DEFAULT_SINK@ +10%
$ pactl set-sink-volume @DEFAULT_SINK@ -10%
```
