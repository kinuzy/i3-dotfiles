#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="tmux"

# https://github.com/tmux-plugins/tpm#installing-plugins 
install(){

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu tmux

        git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
        ln -sf "${DOTFILE_PATH}tmux.conf" ~/.tmux.conf
        info "Press prefix + I (capital i, as in Install) to fetch the plugin."
    else
        warn " - Skip install default packages.."
    fi
}
install
