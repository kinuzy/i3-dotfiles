#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="dwm"

# https://discovery.endeavouros.com/bluetooth/bluetooth/2021/03/
# bluetooth GUI
# https://forum.endeavouros.com/t/black-symbolic-blueberry-tray-icon-under-xfce4/7920
install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu xorg-server xorg-xinit xorg-xsetroot nitrogen picom
        cp /etc/X11/xinit/xinitrc ~/.xinitrc

    else
        warn " - ${command} already installed..."
    fi
}

