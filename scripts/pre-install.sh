#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

useradd -m -g users -G wheel jun
passwd jun


pacman -S base base-devel linux linux-headers linux-lts linux-lts-headers networkmanager ntp \
    grub efibootmgr linux-firmware base-devel man-db man-pages dosfstools e2fsprogs

ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
ntpdate time.kriss.re.kr
hwclock -w


sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
sed -i 's/#ko_KR.UTF-8/ko_KR.UTF-8/' /etc/locale.gen


locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
echo archbox > /etc/hostname
sudo sed -i 's/#Color/Color/' /etc/pacman.conf

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch --recheck
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager

info " \$ exit"
info " \$ umount -lR /mnt"
info "  => then reboot"

