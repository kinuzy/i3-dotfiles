#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="gotop"

install(){

    if ! command_exists "${command}" ; then
        title "install default packages"
        yay -S --noconfirm --nocleanmenu --nodiffmenu \
            mesa xf86-video-amdgpu libva-mesa-driver wget curl amd-ucode \
            firefox alacritty fontconfig ttf-ubuntu-font-family google-chrome openssh \
            youtube-dl rofi gedit transmission-gtk flameshot \
            peek mpv tmux hardinfo ntp asciinema net-tools cmus imagewriter \
            lf ttf-d2coding pyenv direnv pyenv-virtualenv ruby ruby-colorls neofetch \
            tig yarn ripgrep fd git-delta ranger the_silver_searcher unzip libxft-bgra \
            powerline-fonts ttf-fira-code xbindkeys xbindkeys_config-gtk2 acpilight \
            gotop notion-app bitwarden-bin inetutils xclip thunar gvfs ntfs-3g

        git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
        ~/.fzf/install

    else
        warn " - Skip install default packages.."
    fi
}
install
