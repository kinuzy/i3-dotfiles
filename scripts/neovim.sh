#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="nvim"

install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu neovim
        git clone git@gitlab.com:imnuz/settings/nvim.git ~/.config/nvim
    else
        warn " - ${command} already installed..."
    fi
}

install
