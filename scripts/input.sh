#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="fcitx"

install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"

        yay -S fcitx-hangul fcitx-configtool
        echo "export QT_IM_MODULE=fcitx" >> /etc/X11/xinit/xinitrc
        echo "export GTK_IM_MODULE=fcitx" >> /etc/X11/xinit/xinitrc
        echo "export XIM=fcitx" >> /etc/X11/xinit/xinitrc
        echo "export XMODIFIERS=@im=fcitx" >> /etc/X11/xinit/xinitrc

        localectl set-x11-keymap kr pc104 kr104로 키맵 변경. 노트북이 아니라 한글 106 키보드일 경우에는 106 키보드로 진행 해도 좋음.
        info "Search Input Method 항에 hangul 검색해서 더블클릭으로 추가."
        fcitx-configtool
    else
        warn " - ${command} already installed..."
    fi
}

install

