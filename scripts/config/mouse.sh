#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

sudo sed -i 's/pointer catchall"/pointer catchall"\n\tOption "NaturalScrolling" "true"/' /usr/share/X11/xorg.conf.d/40-libinput.conf
sudo sed -i 's/touchpad catchall"/touchpad catchall"\n\tOption "NaturalScrolling" "true"/' /usr/share/X11/xorg.conf.d/40-libinput.conf

#https://wiki.archlinux.org/index.php/Touchpad_Synaptics
#Option      "CircularScrolling"          "on"
#Option      "CircScrollTrigger"          "0"
#Option "Tapping" "on"
