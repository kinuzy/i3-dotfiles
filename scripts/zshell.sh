#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="zsh"

install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu zsh zsh-completions

        chsh -s `which zsh`
        sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        sed -i 's/plugins=(git)/plugins=(archlinux git fzf history-substring-search zsh-autosuggestions zsh-syntax-highlighting)/' ~/.zshrc
        sed -i 's/robbyrussell/spaceship/' ~/.zshrc

        git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1
        ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme" 

        cd ~/.oh-my-zsh/plugins/
        git clone https://github.com/zsh-users/zsh-autosuggestions
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git


        echo 'source ~/.dotfiles/zshrc' >> ~/.zshrc
    else
        warn " - ${command} already installed..."
    fi
}

