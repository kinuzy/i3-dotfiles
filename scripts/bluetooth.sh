#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="docker"

# https://discovery.endeavouros.com/bluetooth/bluetooth/2021/03/
# bluetooth GUI
# https://forum.endeavouros.com/t/black-symbolic-blueberry-tray-icon-under-xfce4/7920
install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu bluez bluez-utils blueberry pulseaudio-bluetooth

        sudo systemctl start bluetooth
        sudo systemctl enable bluetooth
        sudo systemctl enable --now bluetooth

        # Auto Start bluetooth
        sed -i 's/#AutoEnable/AutoEnable/' /etc/bluetooth/main.conf
    else
        warn " - ${command} already installed..."
    fi
}

