#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

# install_packages
source "./packages.sh"
source "./bluetooth.sh"
source "./ccat.sh"
source "./yay.sh"
source "./input.sh"
source "./neovim.sh"
source "./tmux.sh"
source "./config/font.sh"
source "./config/mouse.sh"
source "./config/timezone.sh"
