#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="alsactl"
    

# https://discovery.endeavouros.com/bluetooth/bluetooth/2021/03/
# bluetooth GUI
# https://forum.endeavouros.com/t/black-symbolic-blueberry-tray-icon-under-xfce4/7920
install () {

    if ! command_exists "${command}" ; then
        title "install alsa"
        yay -S --noconfirm --nocleanmenu --nodiffmenu alsa alsa-utils

    else
        warn " - alsa already installed..."
    fi
}

