#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="virtualbox"

install (){

    if ! command_exists "${command}" ; then
        title "install ${command}"
        git clone https://aur.archlinux.org/yay.git
        mv ygy .yay 
        cd .yay
        makepkg -si
        yay -Syu
    else
        warn " - ${command} already installed..."
    fi
}
install
