#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="virtualbox"

install_virtualbox () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu \
            virtualbox \
            linux52-virtualbox-host-modules \
            linux52-virtualbox-guest-modules
        sudo vboxreload
        info "visit http://download.virtualbox.org/virtualbox/6.0.12/ to downlaod guest-iso file."
    else
        warn " - ${command} already installed..."
    fi
}

install_virtualbox
