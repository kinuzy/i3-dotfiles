#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="ccat"

install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        wget https://github.com/owenthereal/ccat/releases/download/v1.1.0/linux-amd64-1.1.0.tar.gz
        tar xfz linux-amd64-1.1.0.tar.gz 
        sudo cp linux-amd64-1.1.0/ccat /usr/local/bin/
        sudo chmod +x /usr/local/bin/ccat
        rm -fr linux-amd64*
    else
        warn " - ${command} already installed..."
    fi
}

install
