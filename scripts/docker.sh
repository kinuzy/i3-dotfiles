#!/bin/bash 
# shellcheck source=/dev/null
source "./_lib.sh"

command="docker"

install () {

    if ! command_exists "${command}" ; then
        title "install ${command}"
        yay -S --noconfirm --nocleanmenu --nodiffmenu docker docker-compose
        sudo usermod -aG docker $USER
        sudo systemctl enable docker
        sudo systemctl start docker
    else
        warn " - ${command} already installed..."
    fi
}

install
