# -----------------------------------------------------------------------------------------------
# alias
# -----------------------------------------------------------------------------------------------
alias ls='colorls --sort-dirs '
alias l='colorls --sort-dirs --report -l'
alias ll='colorls --sort-dirs --report -al'
alias vi=nvim
alias vim=nvim
alias v=nvim
alias cat=ccat
alias lzg=lazygit
alias cls=clear


# https://denysdovhan.com/spaceship-prompt/docs/Options.html#exit-code-exit_code
SPACESHIP_PROMPT_ADD_NEW_LINE=true
SPACESHIP_PROMPT_DEFAULT_PREFIX="$USER"
SPACESHIP_PROMPT_FIRST_PREFIX_SHOW=true
SPACESHIP_CHAR_SYMBOL="\uf0e7"
SPACESHIP_CHAR_PREFIX="\uf296"
SPACESHIP_CHAR_SUFFIX=(" ")
SPACESHIP_USER_SHOW=true
SPACESHIP_TIME_SHOW=true
SPACESHIP_PYENV_SYMBOL="\ue73c"
SPACESHIP_PYENV_COLOR="green"


# -----------------------------------------------------------------------------------------------
# Environment
# -----------------------------------------------------------------------------------------------
export EDITOR=nvim
export PYENV_ROOT=$HOME/.pyenv
export PYENV_PATH=$PYENV_ROOT
if which pyenv > /dev/null; then eval "$(pyenv init --path)"; fi
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi


# -----------------------------------------------------------------------------------------------
# auto tmux
# -----------------------------------------------------------------------------------------------
# case $- in *i*)
#   [ -z "$TMUX" ] && exec tmux
# esac


# -----------------------------------------------------------------------------------------------
# zsh
# -----------------------------------------------------------------------------------------------
source ~/.oh-my-zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.oh-my-zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


# -----------------------------------------------------------------------------------------------
# fzf
# -----------------------------------------------------------------------------------------------
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
if [ -f /usr/share/fzf/key-bindings.zsh ];then
    source /usr/share/fzf/key-bindings.zsh
    source /usr/share/fzf/completion.zsh
fi
export ZFZ_DEFAULT_OPTS="--extended"
export FZF_DEFAULT_COMMAND="fd --type f"

neofetch
